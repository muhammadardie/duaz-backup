### Requirements ###
* git
* composer
* php 7.2 or higher

### Installation ###
* create a folder 'duaz-backup'
* `cd duaz-backup`
* `git init`
* `git remote add origin https://muhammadardie@bitbucket.org/muhammadardie/duaz-backup.git`
* `git fetch && git checkout master`
* `git pull`
* make sure `Already up to date.`
* `composer install`
* `cp .env.example .env`
* `php artisan key:generate`
* Create a database and inform *.env*
* `php artisan migrate --seed` to create and populate tables
* make sure folder storage and bootstrap writable
### Login ###

* email = `locolo@mail.com`
* password = `123456`

### Push to repository ###
* `git add .`
* `git commit -m 'your commit message here'`
* `git push origin master`